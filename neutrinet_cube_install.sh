#!/bin/bash

# Neutrinet SD Card Installer
# Copyright (C) 2019 Ilja Baert <neutrinet@spectraltheorem.be>
# Parts of this script are based on and/or copied from neutrinet.sh and 
# install-sd.sh from the Neutrinet and labriqueinternet projects.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#=================================================
# FUNCTIONS
#=================================================

function show_welcome() {
  clear
  cat <<EOF
********************************************************************************
You are about to configure a Neutrinet Internet Cube. If you do
not know what this is, please visit https://neutrinet.be/en/brique
for more information.

Before you start this script, we assume you have the following:
* Either an A20-OLinuXino-LIME or an A20-OLinuXino-LIME2 board
* 5V DC power supply for the board
* Minimum 16G micro sd card
* Wifi USB antenna
* Preferably a case
* Optionally a neutrinet VPN account and certificates

You also need an internet connection on this machine while running the script and
an ethernet connection for the cube. Preferably the cube will be on the same local
network as this machine. You also need to be able to connect the micro sd card to 
this machine.

/!\\ Be aware that as soon as the vpn goes live the root user can log in over
    the vpn with the chosen password! Choosing a dictionary word or 12345678 is
    not the best thing to do here, instead have a look at
    https://ssd.eff.org/en/module/creating-strong-passwords for advice on
    creating a strong password.

Press ENTER to continue or CTRL-C to abort
EOF
  read -s
}

function get_hypercube_file() {

  echo
  echo "What domain name do you want to use? (will be used to host your email and services)"
  echo "You can either use your own domain, or freely use a subdomain under one of the following domains:"
  echo "  .nohost.me"
  echo "  .noho.st"
  echo "  .ynh.fr"
  echo "i.e.: example.nohost.me"
  read domain
  domain_available=false
  while ( ! $domain_available )
  do
    if [ "$(echo $domain | grep .noho.st$ || echo $domain | grep .nohost.me$ || echo $domain | grep .ynh.fr$)" != "" ]
    then # ynh dyndns
      echo "you've chosen one of the subdomain. Checking availability..."
      ( curl -sI "https://dyndns.yunohost.org/test/$domain" | grep HTTP | grep 200 > /dev/null ) && domain_available=true \
        && echo "$domain is available!"
    else # own domain
      domain_available=true
    fi
    if (! $domain_available )
    then
      echo
      echo "$domain is not available. Please choose another domain"
      echo "You can either use your own custom domain, or freely use a subdomain under one of the following doamains:"
      echo "  .nohost.me"
      echo "  .noho.st"
      echo "  .ynh.fr"
      echo "i.e.: example.nohost.me"
      read domain
    fi
  done

  echo
  echo "Username (used to connect to the user interface and access your apps, must be composed of lowercase letters and numbers only)"
  echo "i.e.: jonsnow"
  read username
  while [ "$username" == "" ] # TODO: test with regex
  do
    echo "Please provide a username (used to connect to the user interface and access your apps, must be composed of lowercase letters and numbers only)"
    echo "i.e.: jonsnow"
    read username
  done

  echo
  echo "Firstname (mandatory, used as your firstname when you send emails)"
  echo "i.e.: Jon"
  read firstname
  while [ "$firstname" == "" ]
  do
    echo "Please provide a firstname (mandatory, used as your firstname when you send emails)"
    echo "i.e.: Jon"
    read firstname
  done

  echo
  echo "Lastname (mandatory, used as your lastname when you send emails)"
  echo "i.e. Snow"
  read lastname
  while [ "$lastname" == "" ]
  do
    echo "Please provide a lastname (mandatory, used as your lastname when you send emails)"
    echo "i.e. Snow"
    read lastname
  done

  echo
  echo "user password (Use a strong password!)"
  echo "see https://ssd.eff.org/en/module/creating-strong-passwords for advice"
  read -s user_pwd

  echo
  echo "admin password (Use a strong password! This will access the admin screen of your internetcube)"
  echo "see https://ssd.eff.org/en/module/creating-strong-passwords for advice"
  read -s admin_pwd

  # The following should be automated
  # See https://github.com/Neutrinet/scripts/tree/master/vpn for inspiration
  echo
  echo "You will now need to enter your neutrinet vpn certificates and credentials"
  echo "If you want to reuse certificates from a previous install, you can find everything on that cube as user.crt, user.key, ca-server.crt and credentials in /etc/openvpn/keys"
  echo "If you do not have an account yet, please see https://wiki.neutrinet.be/vpn/order"
  echo
  echo "VPN client certificate (paste all the content of client.crt below and end with a blank line): "
  vpn_client_crt=$(sed '/^$/q' | sed '/^$/d')
  echo
  echo "VPN client key (paste all the content of client.key below and end with a blank line): "
  vpn_client_key=$(sed '/^$/q' | sed '/^$/d')
  echo
  echo "CA server certificate (paste all the content of ca.crt below and end with a blank line): "
  vpn_ca_crt=$(sed '/^$/q' | sed '/^$/d')
  echo
  echo "VPN username (first line of the 'auth' file): "
  read vpn_username
  echo
  echo "VPN password (second line of the 'auth' file): "
  read -s vpn_pwd
  echo
  echo "ALRIGHT! That was it for the certificates ^^ Let's continue with the rest, shall we"
  echo

  echo
  echo "WiFi hotspot name"
  echo "i.e.: MyWunderbarNeutralNetwork"
  read wifi_ssid
  echo
  echo "WiFi hotspot password"
  echo "i.e.: MyWunderbarNeutralNetwork"
  read -s wifi_pwd
  echo

  create_hypercubefile
}

function create_hypercubefile() {
  echo
  echo "Creating hypercubefile..."

  # Make template file
  hypercubefile="install.hypercube"

  cat > $hypercubefile << EOF
{
  "vpnclient": {
    "server_name": "vpn.neutrinet.be",
    "server_port": "1195",
    "server_proto": "udp",
    "ip6_net": "",
    "ip4_addr": "",
    "crt_server_ca": "VPN_CA_CRT",
    "crt_client": "VPN_CLIENT_CRT",
    "crt_client_key": "VPN_CLIENT_KEY",
    "crt_client_ta": "",
    "login_user": "VPN_USERNAME",
    "login_passphrase": "VPN_PWD",
    "dns0": "89.234.141.66",
    "dns1": "2001:913::8",
    "openvpn_rm": [
      ""
    ],
    "openvpn_add": [
      "cipher AES-256-CBC",
      "tls-version-min 1.2",
      "auth SHA256",
      "topology subnet"
    ]
  },
  "hotspot": {
    "wifi_ssid": "WIFI_SSID",
    "wifi_passphrase": "WIFI_PWD",
    "ip6_net": "",
    "ip6_dns0": "2001:913::8",
    "ip6_dns1": "2001:910:800::40",
    "ip4_dns0": "80.67.188.188",
    "ip4_dns1": "80.67.169.12",
    "ip4_nat_prefix": "10.0.242",
    "firmware_nonfree": "no"
  },
  "yunohost": {
    "domain": "DOMAIN",
    "password": "ADMIN_PWD",
    "user": "USERNAME",
    "user_firstname": "FIRSTNAME",
    "user_lastname": "LASTNAME",
    "user_password": "USER_PWD"
  },
  "unix": {
    "root_password": "ADMIN_PWD",
    "lang": "en"
  }
}
EOF

  # Prepare vars to replace in file
  vpn_client_crt="${vpn_client_crt//$'\n'/\\|}"
  vpn_client_key="${vpn_client_key//$'\n'/\\|}"
  vpn_ca_crt="${vpn_ca_crt//$'\n'/\\|}"

  # Replace vars in file
  sed -i "s~\"VPN_CLIENT_CRT\"~\"$vpn_client_crt\"~" $hypercubefile
  sed -i "s~\"VPN_CLIENT_KEY\"~\"$vpn_client_key\"~" $hypercubefile
  sed -i "s~\"VPN_CA_CRT\"~\"$vpn_ca_crt\"~" $hypercubefile
  sed -i "s/VPN_USERNAME/$vpn_username/g" $hypercubefile
  sed -i "s/VPN_PWD/$vpn_pwd/g" $hypercubefile

  sed -i "s/WIFI_SSID/$wifi_ssid/g" $hypercubefile
  sed -i "s/WIFI_PWD/$wifi_pwd/g" $hypercubefile
  sed -i "s/DOMAIN/$domain/g" $hypercubefile
  sed -i "s/USERNAME/$username/g" $hypercubefile
  sed -i "s/FIRSTNAME/$firstname/g" $hypercubefile
  sed -i "s/LASTNAME/$lastname/g" $hypercubefile
  sed -i "s/USER_PWD/$user_pwd/g" $hypercubefile
  sed -i "s/ADMIN_PWD/$admin_pwd/g" $hypercubefile

  echo "install.hypercube created"
}

function get_image() {
  echo
  echo "What hardware are you installing on? lime/lime2"
  read board
  while [ "$board" != "lime" ] && [ "$board" != "lime2" ]
  do
    echo "This script doesn't support this. Please choose lime/lime2"
    read board
  done

  download_image
}

function download_image() {
  echo
  echo "Finding image..."

  image=$(ls ../cube_resources/internetcube*-$board-*.img 2> /dev/null | head -n1)
  if [ -z $image ]
  then
    image=$(curl -s $YNH_IMG_LOCATION | grep .img.zip\" | grep "stretch" | grep $board- | perl -nle 'm/href="([^ ]*).zip"/; print $1';)
    # TODO
    # error if no image is found. Alternative is to use a fallback. We can also first check the folder from where it's executed. If there's a correct file there=>ask to use that one? No gpg check needed then. What if multiple images are found?
    # check gpg (download it and check it during flashing with the install-sd.sh script)
    echo "downloading $board image"
    curl -#o "$image.zip"  "$YNH_IMG_LOCATION/$image.zip" # This should have the -s option instead of -# if we make it async

    echo
    echo "unzipping..."
    unzip "$image.zip"
    rm "$image.zip"
    image=$(ls *-$board-*.img | head -n1)

    echo
    echo "preparing as internetcube image..."
    check_sudo
    [ -d yunocube ] || git clone "$CUBE_BUILD_SCRIPT_LOCATION" "yunocube" && sudo bash "yunocube/yunocube.sh" "$image"
    rm "$image"
    image=$(ls internetcube*-$board-*.img | head -n1)
  fi
  echo "We've got it!"
}

function flash_sd_card() {
  echo
  echo "Let's flash the SD-card!"
  [ -f install-sd.sh ] || curl -so "install-sd.sh" "$INSTALL_SD_SCRIPT_LOCATION"
  bash install-sd.sh -y "$hypercubefile" -f "$image"
}

function prepare_cube() {
  echo
  echo "Alright! Time to prepare the cube!"
  echo "Make sure the Cube is not connected to a power source (e.g: the power cable should be unplugged)"
  echo "Assemble the board and case"
  echo "Insert the WiFi antenna into the USB port of the Cube's board"
  echo "Insert the SD card into your Cube"
  echo "Connect your Cube to an ethernet cable that is connected to the internet"
  echo "Connect the power cable to the Cube to start it"
  echo "It may taken a couple of minutes before the cube has started up an is connected to the internet"
  echo "Once the cube has started up, it will setup everything based on the answers that where given"
  echo "Compleet setup can take up to an hour or even more. Please be patient. It should not take two hours"
  echo "By the time the setup is compleet, you will be able to connect through the vpn"
  echo 
  echo "Press ENTER when the cube is connected"
  read -s
}

function search_cube() {
  [ -f "install-sd.sh" ] || curl -so "install-sd.sh" "$INSTALL_SD_SCRIPT_LOCATION"
  echo
  echo "Once your cube is started up, we can search for it on the local network so you can follow it's progress"
  echo "Note that it can take a couple of minutes before the cube is discoverable"
  echo "Once found, you can follow the progress from the HyperCube Debug url which is of the form http://<local-ip>:2486/install.html"
  echo "Note that even after finding it it can take a minute or so before the HyperCube Debug url works"
  echo "You can also easily add the ip to your hosts file"
  echo "Do you want to search for your cube on the local network? (yes/no)"
  read search_cubes
  if ! [ "$search_cubes" == "no" ]
  then
    echo "Searching for cubes"
    continue_searching="yes"
    while ! [ "$continue_searching" == "no" ]
    do
      bash ./install-sd.sh -l
      echo "If we found it you should be able to follow the progress at the HyperCube Debug url"
      echo "Do you want to continue searching? (yes/no)"
      read continue_searching
    done
    echo
    echo "Ok! Note that the 'HyperCube Debug' url will be accessable up to four hours after completion"
  fi
}

function show_goodbye(){
  echo
  echo "Alright, that's all we could do for now. We hope you'll be happy with your cube!"
  echo "If you have problems, don't hesitate to reach out to us at"
  echo "* Our wiki: https://wiki.neutrinet.be/cube/problems"
  echo "* Our Mattermost: https://chat.neutrinet.be"
  echo "* On freenode: #neutrinet"
  echo "* Via mail: contact@neutrinet.be"
  echo
  echo "And if you want to contribute to this awesome project, don't hesitate to contact us as well ;)"
}

function check_sudo() {
  if ! which sudo &> /dev/null; then
    echo "sudo command is required" && exit 1
  fi

  echo "This script needs a sudo access"

  if ! sudo echo &> /dev/null; then
    echo "sudo password is required" && exit 1
  fi
}

function exit_script() {
  echo
  echo "Cleaning up..."
  [ $DEBUG ] && read
  cd "../"
  rm -rf "$TMP_DIR"
}

function prepare() {
  echo "setting up the cube_resources folder"
  mkdir -p cube_resources && cd cube_resources
  echo "Downloading and preparing the lime image"
  board=lime
  download_image
  echo "Downloading and preparing the lime2 image"
  board=lime2
  download_image
  rm -rf yunocube
  rm -rf tmp
  echo "Downloading the install-sd.sh script"
  curl -so "install-sd.sh" "$INSTALL_SD_SCRIPT_LOCATION"
}

function show_help() {
  echo -e "\n       \e[7m\e[1m Neutrinet Cube installer \e[0m\n"
  echo -e "\e[1mOPTIONS\e[0m"
  echo -e "  \e[1m-p\e[0m" >&2
  echo -e "     Downloads and prepares all general images and files needed for installing the cube" >&2
  echo -e "     \e[2mNext time this script is ran from the same folder as it is ran now, these files and scrips will be used\e[0m" >&2
  echo -e "  \e[1m-h\e[0m" >&2
  echo -e "     Show this help" >&2
}

#=================================================
# GLOBAL VARIABLES
#=================================================
YNH_IMG_LOCATION="https://build.yunohost.org"
CUBE_BUILD_SCRIPT_LOCATION="https://github.com/labriqueinternet/build.labriqueinter.net"
INSTALL_SD_SCRIPT_LOCATION="https://raw.githubusercontent.com/labriqueinternet/labriqueinter.net/master/install-sd.sh"

#=================================================
# GET OPTIONS
#=================================================

while getopts "dhp" opt; do
  case $opt in
    d) DEBUG=true;;
    p) prepare; exit 0 ;;
    h) show_help; exit 0 ;;
    \?) show_help; exit 0 ;;
  esac
done

#=================================================
# SCRIPT
#=================================================

TMP_DIR=$(mktemp -dp . .neutrinetcube_tmpXXXXXX)
# Exit if any of the following command fails
set -e
cd $TMP_DIR
trap exit_script EXIT

show_welcome
get_hypercube_file
get_image
flash_sd_card
prepare_cube
search_cube
show_goodbye

